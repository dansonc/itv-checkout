package ITV;


import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;


public class EposTest {

    private Epos epos;

    private final Map<Sku, PriceRetriever> priceRetrievers = new HashMap<>();

    private final PriceRetriever newPriceRetrieverForItemA = new BulkPurchase(new BigDecimal("0.40"), 2);

    private final Sku skuA = new Sku(new BigDecimal("0.50"));
    private final Sku skuB = new Sku(new BigDecimal("0.30"));
    private final Sku skuC = new Sku(new BigDecimal("2.00"));
    private final Sku skuD = new Sku(new BigDecimal("0.75"));
    private final Sku skuE = new Sku(new BigDecimal("1.00"));

    @Before
    public void setup(){

        priceRetrievers.put(skuA, new BulkPurchase(new BigDecimal("0.30"), 3));
        priceRetrievers.put(skuB, new BulkPurchase(new BigDecimal("0.15"), 2));
        priceRetrievers.put(skuC, new StandardPrice());
        priceRetrievers.put(skuD, new StandardPrice());

        epos = new Epos(priceRetrievers);
    }

    @Test
    public void scanOneItemA(){

        scanItems(skuA);

        assertThat(epos.getTotal(), is(new BigDecimal("0.50")));
    }

    @Test
    public void scanTwoItemA(){

        scanItems(skuA, skuA);

        assertThat(epos.getTotal(), is(new BigDecimal("1.00")));
    }

    @Test
    public void scanThreeItemA(){

        scanItems(skuA, skuA, skuA);

        assertThat(epos.getTotal(), is(new BigDecimal("1.30")));
    }

    @Test
    public void scanOneItemB(){

        scanItems(skuB);

        assertThat(epos.getTotal(), is(new BigDecimal("0.30")));
    }

    @Test
    public void scanTwoItemB(){

        scanItems(skuB, skuB);

        assertThat(epos.getTotal(), is(new BigDecimal("0.45")));
    }

    @Test
    public void scanThreeItemB(){

        scanItems(skuB, skuB, skuB);

        assertThat(epos.getTotal(), is(new BigDecimal("0.75")));
    }

    @Test
    public void scanOneItemC(){

        scanItems(skuC);

        assertThat(epos.getTotal(), is(new BigDecimal("2.00")));
    }

    @Test
    public void scanOneItemD(){

        scanItems(skuD);

        assertThat(epos.getTotal(), is(new BigDecimal("0.75")));
    }

    @Test
    public void scanTwoItemBOneItemCAndOneItemD(){

        scanItems(skuB, skuB, skuC, skuD);

        assertThat(epos.getTotal(), is(new BigDecimal("3.20")));
    }

    @Test
    public void scanOneItemBOneItemCOneItemDOneItemB_InOrder(){

        scanItems(skuB, skuC, skuD, skuB);

        assertThat(epos.getTotal(), is(new BigDecimal("3.20")));
    }

    @Test (expected = MissingPriceRetrieverException.class)
    public void scanItemWithoutPriceRetriever() throws MissingPriceRetrieverException, InvalidOfferException {

        epos.scan(skuE);
    }

    @Test
    public void totalWithoutScanningIsZero(){

        assertThat(epos.getTotal(), is(new BigDecimal("0.00")));
    }

    @Test
    public void scanTwoItemA_WithDifferentPriceRetrievers(){

        scanItems(skuA);
        epos.setNewPriceRetriever(skuA, newPriceRetrieverForItemA);
        scanItems(skuA);

        assertThat(epos.getTotal(), is(new BigDecimal("0.90")));
    }

    @Test(expected = InvalidOfferException.class)
    public void offerPriceGreaterThanStandardPriceThrowsException() throws MissingPriceRetrieverException, InvalidOfferException {

        epos.setNewPriceRetriever(skuB, newPriceRetrieverForItemA );
        epos.scan(skuB);
    }

    private void scanItems(Sku... skus) {

        Arrays.stream(skus).forEach(sku -> {
            try {
                epos.scan(sku);
            } catch (MissingPriceRetrieverException e) {
                fail("MissingPriceRetriever");
            } catch (InvalidOfferException e) {
                fail("Invalid Offer");
            }
        });
    }
}
