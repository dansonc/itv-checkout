package ITV;

import java.math.BigDecimal;

public class StandardPrice implements PriceRetriever {

    @Override
    public BigDecimal getPrice(Sku sku, int scannedCount) {
        return sku.getPrice();
    }
}
