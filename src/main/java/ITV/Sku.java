package ITV;

import java.math.BigDecimal;

public class Sku {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sku sku = (Sku) o;

        return standardPrice.equals(sku.standardPrice);
    }

    @Override
    public int hashCode() {
        return standardPrice.hashCode();
    }

    private BigDecimal standardPrice;

    public Sku(BigDecimal standardPrice) {

        this.standardPrice = standardPrice;
    }

    public BigDecimal getPrice() {
        return standardPrice;
    }
}
