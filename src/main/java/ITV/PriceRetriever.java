package ITV;

import java.math.BigDecimal;

interface PriceRetriever {

    BigDecimal getPrice(Sku sku, int scannedCount) throws InvalidOfferException;
}
