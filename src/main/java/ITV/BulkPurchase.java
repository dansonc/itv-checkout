package ITV;

import java.math.BigDecimal;

public class BulkPurchase implements PriceRetriever {

    private final BigDecimal offerPrice;
    private final int requiredQuantity;

    BulkPurchase(BigDecimal offerPrice, int requiredQuantity) {
        this.offerPrice = offerPrice;
        this.requiredQuantity = requiredQuantity;
    }

    @Override
    public BigDecimal getPrice(Sku sku, int scannedCount) throws InvalidOfferException {

        confirmOfferPriceIsLessThanStandardPrice(sku);

        if(scannedCount % requiredQuantity == 0){
            return offerPrice;
        }

        return sku.getPrice();
    }

    private void confirmOfferPriceIsLessThanStandardPrice(Sku sku) throws InvalidOfferException {
        if(sku.getPrice().compareTo(offerPrice) < 0){
            throw new InvalidOfferException();
        }
    }
}
