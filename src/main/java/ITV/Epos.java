package ITV;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static java.math.BigDecimal.ROUND_UNNECESSARY;

class Epos {

    private final Map<Sku, PriceRetriever> priceRetrievers;
    private BigDecimal total = new BigDecimal("0").setScale(2, ROUND_UNNECESSARY);
    private Map<Sku, Integer> scannedItems = new HashMap<>();


    Epos(Map<Sku, PriceRetriever> priceRetrievers) {

        this.priceRetrievers = priceRetrievers;
    }

    BigDecimal getTotal() {
        return total;
    }

    void setNewPriceRetriever(Sku sku, PriceRetriever newPriceRetriever) {

        priceRetrievers.put(sku, newPriceRetriever);
    }

    void scan(Sku sku) throws MissingPriceRetrieverException, InvalidOfferException {

        if(!priceRetrievers.containsKey(sku)){
            throw new MissingPriceRetrieverException();
        }

        store(sku);
        PriceRetriever priceRetriever = priceRetrievers.get(sku);
        Integer alreadyScannedQuantity = scannedItems.get(sku);
        total = total.add(priceRetriever.getPrice(sku, alreadyScannedQuantity)); //.setScale(2, ROUND_UNNECESSARY);
    }

    private void store(Sku sku) {

        scannedItems.put(sku, scannedCount(sku) + 1);
    }

    private Integer scannedCount(Sku sku) {

        return scannedItems.getOrDefault(sku, 0);
    }
}
