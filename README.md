#README

Extra Points - I am not sure how to interpret 'a single transaction'.
Is it [1] this session at the till ? or [2] this individual item I am scanning.

I am going to go with option [2].

Another point of business logic would be :: on receiving a new PriceRetriever for an Item
during a session at the till, do we [A] want to apply the new PriceRetriever to the already
scanned items, or [B] apply it to future scanned items only.

I am going with option [B] here. Option [A] would be easy enough though and just require a
bit of re-calculation.

The unit test for this behaviour passes in a real map of PriceRetrievers and then this is
updated after scanning the first of the 2 items.


